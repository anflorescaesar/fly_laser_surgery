import pygame
import rospy
from settings import *
import numpy as np

class Joystick:
    def __init__(self, ID, NAME):
        pygame.init()
        pygame.joystick.init()
        self.id = ID
        self.name = NAME
        self.joystick = pygame.joystick.Joystick(self.id)
        self.joystick.init()

        name = self.joystick.get_name()
        rospy.loginfo("Joystick name:     {}".format(name))
        axes = self.joystick.get_numaxes()
        rospy.loginfo("Number of axes:    {}".format(axes))
        buttons = self.joystick.get_numbuttons()
        rospy.loginfo("Number of buttons: {}".format(buttons))

        self.XYZspeedVals = [0,0,0]



    def getVelocityCmd(self):
        self.XYZspeedVals = [0,0,0]

        tuning_velocity = float(-self.joystick.get_axis(JOYSTICK_TUNE_SPEED_AXIS) + 1) / 2



        self.XYZspeedVals[0] = float(self.joystick.get_axis(JOYSTICK_X_AXIS) * tuning_velocity)
        self.XYZspeedVals[1] = float(self.joystick.get_axis(JOYSTICK_Y_AXIS) * tuning_velocity)
        self.XYZspeedVals[2] = float(self.joystick.get_hat(JOYSTICK_Z_AXIS_HAT)[1] *  tuning_velocity)
        return np.array(self.XYZspeedVals)
