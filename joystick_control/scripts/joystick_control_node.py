#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from joystick import Joystick
import numpy as np
from settings import *
import pygame


class JoystickControl:
    def __init__(self, JOYSTICK_ID, JOYSTICK_NAME):
	self.debug("INFO", "joystick_control_node is running!")
        self.joystick = Joystick(JOYSTICK_ID, JOYSTICK_NAME)
        ####### publisher ##########
        self.velCmdPub = rospy.Publisher('/joystick/' + self.joystick.name + "/velocity_cmd", JointState, queue_size=10)
        self.velCmdMsg = JointState()

    """ Code for the main thread of the node """
    def mainThread(self):
        for event in pygame.event.get():  # NECESSARY FOR JOYSTICK
            pass


        vel_cmdTrans = self.joystick.getVelocityCmd()
        vel_cmdTrans = MAX_SPEED_TRANSLATIONAL_JOINTS * vel_cmdTrans
        vel_cmdTrans[np.abs(vel_cmdTrans) < MIN_SPEED] = 0
        velcmd = vel_cmdTrans


        self.velCmdMsg.name = ['X', 'Y', 'Z']
        self.velCmdMsg.velocity = velcmd
        self.velCmdMsg.position = np.zeros(velcmd.shape)
        self.velCmdMsg.header.stamp = rospy.Time.now()
        self.velCmdPub.publish(self.velCmdMsg)



    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('joystick_control_node', anonymous=True)
        rate = rospy.Rate(50)    # 10 Hz

        joystick_id = rospy.get_param('~id')
        joystick_name = rospy.get_param('~name')

        node = JoystickControl(joystick_id, joystick_name)

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

