#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
import time
import actionlib
import micromanipulator_interface.msg as mi
from micromanipulator_interface.srv import set_calibration, set_calibrationResponse
from micromanipulator_interface.srv import get_info, get_infoResponse
import numpy as np
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import sys
import cv2


FONT = cv2.FONT_HERSHEY_SIMPLEX
FONTSCALE = 1
LINE_HEIGHT = 50
COLOR_PROGRAM = (0,177, 177)
COLOR_FONT = (255, 255, 255)
COLOR_STATE = (177, 0, 0)
COLOR_INFO  = (0,0,255)
STATES       = ['STOP', 'MANUAL', 'AUTO']
COMMAND_LIST = ['help', 'clr', 'p', 'g+', 'g', 'w', 'm', 'j']
TYPECMD = '>> '
RESOLUTION =  [1500, 1500]


VELOCITY_P = 1000
VELOCITY_G = 500


PUBLISH_RATE_VEL_CMD = 10







def getNextIx(currentIx, maxIx):
    currentIx += 1
    if currentIx == maxIx:
        currentIx = 0
    return currentIx


def convertStrArray2Array(arr):
    arr = arr.split('[')[1]
    arr = arr.split(']')[0]
    arr_int = []
    for num in arr.split(' '):
        if num != '':

            arr_int.append(float(num))
    return arr_int


def getTrajectoryMsg(positions, velocities):
    trajectoryMsg = JointTrajectory()
    trajectoryMsg.header.stamp = rospy.Time.now()
    trajectoryMsg.joint_names = ['X', 'Y', 'Z', 'G']
    trajectoryMsg.points.append(JointTrajectoryPoint())
    trajectoryMsg.points[0].accelerations = np.zeros(len(positions))
    trajectoryMsg.points[0].velocities = velocities
    trajectoryMsg.points[0].positions = positions
    return trajectoryMsg











class MicromanipulatorCmd:
    def __init__(self, manipulator_name, joystick_name):
	self.debug("INFO", "micromanipulator_cmd_node is running!")

        self.state = STATES[0]

        self.manipulator_name = manipulator_name
        self.joystick_name    = joystick_name

        self.manipulatorState = JointState()
        ####### publisher ##########
        self.velocityCmdPub = rospy.Publisher(self.manipulator_name + "/motors/velocity_cmd", JointState , queue_size=10)

        ####### subscribers ########
        rospy.Subscriber(self.manipulator_name + "/motors/motors_state", JointState, self.motorStateCB)
        rospy.Subscriber('/joystick/' + self.joystick_name + "/velocity_cmd", JointState, self.joystickCB)


        rospy.loginfo("WAITING FOR TO MANIPULATOR " + self.manipulator_name)
        ####### ACTION CLIENT ######
        self.trajectoryClient = actionlib.SimpleActionClient(self.manipulator_name, mi.move2posAction)
        self.trajectoryClient.wait_for_server()

        ######### SERVICES #########
        self.getInfoService = rospy.ServiceProxy(self.manipulator_name + '/get_info', get_info)
        self.getInfoService.wait_for_service()
        self.manipulator_info = self.getInfoService()
        rospy.loginfo("CONNECTED TO MANIPULATOR " + self.manipulator_name)
        rospy.loginfo(self.manipulator_info)




        self.trajectoryMsg = JointTrajectory()
        #self.program = [['p', np.array([200., 0., 0.])], ['p', np.array([0., 200., 0.])], ['p', np.array([0., 0., 200.])]]
        self.program = []
        self.ixProgram = 0

        cv2.namedWindow("CMD " + self.manipulator_name)

        self.typedCommand = ""
        self.waitingForProgram = True
        self.timerPaused = time.time()
        self.timePaused = 0
        self.infoMsg = ''

        self.gripperGoalPos = 0
        self.xyzGoalPos = [0, 0, 0]

        self.publishVelTimer = time.time()
        self.iterationLoop(getKey=False)




    """ call backs functions """
    def  motorStateCB(self, data):
        self.manipulatorState = data

    def joystickCB(self, data):
        if self.state == STATES[1]:
            if time.time() - self.publishVelTimer > 1.0/PUBLISH_RATE_VEL_CMD:
                self.velocityCmdPub.publish(data)
                self.publishVelTimer = time.time()


    """ Code for the main thread of the node """
    def mainThread(self):
        self.iterationLoop(getKey=True)

        if self.state == STATES[2]:       # AUTO
            program_cmd = self.program[self.ixProgram][0]
            program_arg = self.program[self.ixProgram][1]

            if not self.waitingForProgram:

                if program_cmd == COMMAND_LIST[2]:  # p
                    goalXYZPos = np.zeros(len(self.manipulatorState.position))
                    goalXYZPos[:3] = program_arg
                    goalXYZPos[3:] = self.manipulatorState.position[3:]
                    velocities = VELOCITY_P*np.ones(len(self.manipulatorState.velocity))
                    trajectoryMsg = getTrajectoryMsg(goalXYZPos, velocities)
                    goalMsg = mi.move2posGoal()
                    goalMsg.trajectory = trajectoryMsg
                    self.trajectoryClient.send_goal(goalMsg)
                    self.waitingForProgram = True

                if program_cmd in COMMAND_LIST[3:5]:  # g+, g
                    goalGripper = np.array(self.manipulatorState.position)
                    if program_cmd == COMMAND_LIST[3]:
                        goalGripper[3:] += program_arg
                    else:
                        goalGripper[3:] = program_arg
                    velocities = VELOCITY_G*np.ones(len(self.manipulatorState.velocity))
                    trajectoryMsg = getTrajectoryMsg(goalGripper, velocities)
                    goalMsg = mi.move2posGoal()
                    goalMsg.trajectory = trajectoryMsg
                    self.trajectoryClient.send_goal(goalMsg)
                    self.waitingForProgram = True


                if program_cmd == COMMAND_LIST[5]:    # w
                    self.timePaused = program_arg
                    self.timerPaused = time.time()
                    self.waitingForProgram = True




            else:
                if program_cmd in COMMAND_LIST[2:5]:  # p, g+, g
                    result = self.trajectoryClient.get_result()
                    if result is not None:
                        if result.completed:
                            rospy.loginfo('Goal of manipulator ' + self.manipulator_name + ' was finished')
                            self.ixProgram = getNextIx(self.ixProgram, len(self.program))
                            self.waitingForProgram = False
                        else:
                            rospy.logwarn('Goal of manipulator ' + self.manipulator_name + ' was cancelled!')
                            self.infoMsg = 'manipulator goal cancelled'
                            self.state = STATES[0]


                if program_cmd == COMMAND_LIST[5]:  # WAIT
                    result = self.robotPaused()
                    if result:
                        self.ixProgram = getNextIx(self.ixProgram, len(self.program))
                        self.waitingForProgram = False






    def iterationLoop(self, getKey = True):
        self.cmdWin = np.zeros(RESOLUTION + [3], dtype = 'uint8')

        if getKey:
            self.keyPressed()

        for i in range(len(self.program)):
            self.cmdWin = cv2.putText(self.cmdWin, str(i) + ' '*5 + self.program[i][0] + ' ' + str(self.program[i][1]), (20, (i+1)*LINE_HEIGHT), FONT, FONTSCALE, COLOR_PROGRAM, 4)

        self.cmdWin = cv2.putText(self.cmdWin, TYPECMD + self.typedCommand, (20,  (len(self.program)+1)*LINE_HEIGHT), FONT, FONTSCALE, COLOR_FONT, 4)

        self.cmdWin[-200:,:] = [255, 255, 255]


        if self.state == STATES[2]:
            self.infoMsg = 'running line:' + ' '*5 + str(self.ixProgram) + '  ' + self.program[self.ixProgram][0] + ' ' + str(self.program[self.ixProgram][1])
            if self.program[self.ixProgram][0] == COMMAND_LIST[5]: # wait
                self.infoMsg += ' '*10 + 'remaining time: ' + str(int(self.timePaused - int(time.time() - self.timerPaused))) + ' sec'

        self.cmdWin = cv2.putText(self.cmdWin, "Current state: " + self.state, (20,  RESOLUTION[1] - 150), FONT, FONTSCALE, COLOR_STATE, 4)
        self.cmdWin = cv2.putText(self.cmdWin, "Current position: " + str(self.manipulatorState.position), (20,  RESOLUTION[1] - 100), FONT, FONTSCALE, (0,0,0), 4)

        self.cmdWin = cv2.putText(self.cmdWin, self.infoMsg, (20,  RESOLUTION[1] - 50), FONT, FONTSCALE, COLOR_INFO, 4)

        cv2.imshow("CMD " + self.manipulator_name, cv2.resize(self.cmdWin, (500,500)))


    def keyPressed(self):
        res = cv2.waitKey(1)
        try:
            s = repr(chr(res % 256)) if res % 256 < 128 else '?'
            if s[1] != '\\':
                s = s[1]
                self.typedCommand += str(s)

            else:         # delete letter
                if s[2] == 'x':
                    self.typedCommand = self.typedCommand[:-1]

                if s[2] == 'n' or s[2] == 'r':   # return key
                    self.addCmd()

        except:
            pass

    def addCmd(self):
        self.infoMsg = ''

        cmd = self.typedCommand.split(' ')[0]
        foundCmd = False
        for i in range(len(COMMAND_LIST)):
            if cmd == COMMAND_LIST[i]:
                foundCmd = True
                self.actionCmd(cmd)

                self.typedCommand = ""

        if not foundCmd:
            self.infoMsg = 'Command not found!'
            self.typedCommand = ""
            self.iterationLoop(getKey=False)





    def actionCmd(self, cmd):
        if cmd == COMMAND_LIST[0]:       # HELP
            self.infoMsg = 'list of commands: '
            for i in range(len(COMMAND_LIST)):
                self.infoMsg += COMMAND_LIST[i] + ', '
            return

        if cmd == COMMAND_LIST[1]:      # CLR
            self.program = []
            return


        if cmd == COMMAND_LIST[2]:       # POSITION XYZ
            self.program.append([cmd, np.array(self.manipulatorState.position[0:3])])
            return

        if cmd == COMMAND_LIST[3]:       # g+ command
            self.program.append([cmd, float(self.typedCommand[2:])])
            return


        if cmd == COMMAND_LIST[4]:       # g command
            self.program.append([cmd, float(self.typedCommand[1:])])
            return

        if cmd == COMMAND_LIST[5]:       # w command
            self.program.append([cmd, float(self.typedCommand[1:])])
            return

        if cmd == COMMAND_LIST[6]:

            requested_st = self.typedCommand.split(' ')[1]
            setState = ''
            for i in range(len(STATES)):
                if requested_st == STATES[i].lower():
                    setState = STATES[i]


            if setState != '':
                if setState == STATES[0]:
                    self.trajectoryClient.cancel_goal()
                    self.infoMsg = 'Stopped program'
                    self.waitingForProgram = False
                    self.state = setState


                if setState == STATES[1]:
                    self.state = setState

                if setState == STATES[2]:
                    if len(self.program) > 0:
                        self.state = setState
                        self.waitingForProgram = False
                        self.ixProgram = 0

                    else:
                        self.infoMsg = 'You need at least one line in your program'
            else:
                self.infoMsg = 'Wrong mode. Modes are '
                for i in range(len(STATES)):
                    self.infoMsg += STATES[i] + ', '
            return


        if cmd == COMMAND_LIST[7]:
            new_ix = int(self.typedCommand[1:])
            if new_ix >= 0 and new_ix < len(self.program):
                self.waitingForProgram = False
                self.ixProgram = new_ix







    def robotPaused(self):
        r = False
        if time.time() - self.timerPaused > self.timePaused:
            r = True
        return r





    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('micromanipulator_cmd_node', anonymous=True)
        rate = rospy.Rate(30)    # 10 Hz
        try:
            manipulator_name = rospy.get_param('~manipulator_name')
            joystick_name    = rospy.get_param('~joystick_name')
        except:
            manipulator_name = 'feeder'
            joystick_name    = 'manual'
            pass

        node = MicromanipulatorCmd(manipulator_name, joystick_name)

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

