#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from settings import *
from manipulator import Manipulator
import threading
import time
import actionlib
import micromanipulator_interface.msg as mi
from micromanipulator_interface.srv import set_calibration, set_calibrationResponse, set_home, set_enable
from micromanipulator_interface.srv import get_info, get_infoResponse, set_homeResponse, set_enableResponse
from trajectory_msgs.msg import JointTrajectory
import numpy as np

class ManipulatorInterface:
    def __init__(self, PORT):

        self.manipulator = Manipulator(PORT)
        self.manipulator.getInfo()
        self.isManipulatorCalibrated = False
        self.manipulatorCalibration  = np.ones(self.manipulator.numMotors)
        self.manipulatorState        = "stopped"
        self.is_enable               = True

        self.offsetPosition = np.zeros(self.manipulator.numMotors)
        ###### MESSAGES #######
        self.jointStateMsg = JointState()


        ####### PUBLISHERS ######
        self.motorStatePub = rospy.Publisher(self.manipulator.name + "/motors/motors_state", JointState, queue_size=10)

        ####### SUBSCRIBERS ########
        rospy.Subscriber(self.manipulator.name + "/motors/velocity_cmd", JointState, self.velocityCmdCB)


        ####### SERVICES ##########
        self.getInfoService   = rospy.Service(self.manipulator.name + '/get_info', get_info, self.getInfoCB)


        ####### ACTION SERVER  ##########
        self.move2posActionServer = actionlib.SimpleActionServer(self.manipulator.name, mi.move2posAction, execute_cb=self.move2posCB, auto_start=False)
        self.move2posActionServer.start()


        self.manipulator.start()
        self.listening = True
        threading.Thread(target=self.listenManipulator).start()



    """ call backs functions """
    def getInfoCB(self, req):
        info = get_infoResponse()
        info.name        = self.manipulator.name
        info.calibrated  = self.isManipulatorCalibrated
        info.numMotors   = self.manipulator.numMotors
        info.nameMotors  = self.manipulator.nameMotors
        info.calibration = self.manipulatorCalibration
        info.enabled     = self.is_enable
        info.state       = self.manipulatorState
        return info



    def velocityCmdCB(self, data):
        if self.is_enable:
            positionCmd = np.zeros(self.manipulator.numMotors)
            velocityCmd = data.velocity
            self.manipulator.sendCmd(positionCmd, velocityCmd)





    def move2posCB(self, goal):
        rospy.loginfo('RECEIVED TRAJECTORY FOR MANIPULATOR ' + self.manipulator.name)
        resultMsg = mi.move2posResult()
        if self.is_enable:
            traj = goal.trajectory
            cancelledGoal = False

            for point in traj.points:
                pos = np.array(point.positions) + self.offsetPosition
                currentPos = np.array(self.manipulator.positionState)
                posCmd = pos - currentPos

                max_vel = np.max(point.velocities)
                max_pos = np.max(np.abs(posCmd))
                if max_pos != 0:
                    vel = np.true_divide( np.abs(posCmd), max_pos)*max_vel
                    vel = np.clip(vel, 50, None).astype(np.int)

                else:
                    vel = np.array(point.velocities)

                #acc = np.array(point.accelerations)


                vel[posCmd == 0] = 0
                self.manipulator.sendCmd(posCmd, vel)

                dist = 1

                while dist != 0:
                    if self.move2posActionServer.is_preempt_requested() or not self.is_enable:
                        cancelledGoal = True
                        break
                    currentPos = self.manipulator.positionState
                    dist = np.sum(np.power(currentPos - pos, 2))**(0.5)
                    time.sleep(0.01)
                if cancelledGoal:
                    break

            if cancelledGoal:
                posCmd = np.zeros(len(self.manipulator.positionState))
                velCmd = np.zeros(len(self.manipulator.positionState))
                self.manipulator.sendCmd(posCmd, velCmd)
                resultMsg.completed = False
                resultMsg.additionalInfo = 'goal cancelled by client'
                self.move2posActionServer.set_preempted(resultMsg, text=resultMsg.additionalInfo)

            else:
                resultMsg.completed = True
                self.move2posActionServer.set_succeeded(resultMsg)
        else:
            resultMsg.completed = False
            resultMsg.additionalInfo = 'manipulator ' + self.manipulator.name + 'is not enabled!'
            self.move2posActionServer.set_preempted(resultMsg, text=resultMsg.additionalInfo)






    """ Code for the main thread of the node """
    def mainThread(self):
        self.publishState()


        if time.time() - self.manipulator.timer_reciv_data > NOT_DATA_RECEIVED_ARDUINO_TIME:
            rospy.logerr("ARDUINO DATA NOT RECEIVED FROM: " + str(self.manipulator.name))




    def publishState(self):
        self.jointStateMsg.name     = self.manipulator.nameMotors
        self.jointStateMsg.position = self.manipulator.positionState - self.offsetPosition
        self.jointStateMsg.velocity = self.manipulator.velocityState
        self.jointStateMsg.header.stamp = rospy.Time.now()
        self.motorStatePub.publish(self.jointStateMsg)



    def listenManipulator(self):
        while self.listening:
            self.manipulator.listenArduino()
            if np.any(self.manipulator.velocityState):
                self.manipulatorState = "stopped"
            else:
                self.manipulatorState = "moving"
            time.sleep(1./(PUBLISH_RATE*10))



    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('manipulators_interface_node', anonymous=True)
        rate = rospy.Rate(10)    # 10 Hz
        PORT = '/dev/ttyUSB1'

        PORT = rospy.get_param('~port')
        #except:
        #    pass
        node = ManipulatorInterface(PORT)

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()
        node.listening = False

    except rospy.ROSInterruptException:
        node.listening = False
        pass

