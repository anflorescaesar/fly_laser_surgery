import serial
import time
import numpy as np
from settings import *



try:
    import rospy
except:
    pass

def debug(typ, info):
    try:
        if typ == 'WARNINIG':
            rospy.logwarn(info)
        elif typ == 'ERROR':
            rospy.logerr(info)
        elif typ == 'INFO':
            rospy.loginfo(info)
    except:
        print(typ + ": " + info)




class Manipulator:
    def __init__(self, portCOM):
        self.ard = serial.Serial(port= portCOM,
                                 baudrate=ARD_BAUD_RATE,
                                 parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE,
                                 bytesize=serial.EIGHTBITS,
                                 )



        time.sleep(2)




    def getInfo(self):
        self.ard.flushInput()
        self.ard.flushOutput()

        self.ard.write('info\n')
        time.sleep(0.5)
        r = self.ard.readline().split(',')

        if r == '':
            debug("ERROR", "NOT RECEIVED INFO FROM ARDUINO!")

        self.name = r[0]
        self.numMotors = int(r[1])
        self.nameMotors = []
        for i in range(self.numMotors-1):
            self.nameMotors.append(r[2+i])

        self.nameMotors.append(r[2+self.numMotors-1].split('\r')[0])

        debug("INFO", " " * 5 + "Name micromanipulator: " + self.name)
        debug("INFO", " " * 5 + "Number of motors:      " + str(self.numMotors))
        debug("INFO", " " * 5 + "Name of the motors:    " + str(self.nameMotors) + "\n")


    def start(self):
        self.ard.write('start ' + str(RECEIVING_RATE) + '\n')   # Send arduino cmd to send motors state
        self.positionState = np.zeros(self.numMotors)
        self.velocityState = np.zeros(self.numMotors)
        self.timer_reciv_data = time.time()



    def listenArduino(self):
        cb = self.ard.readline()
        if cb != '':
            try:
                cb = cb.split(',')
                if len(cb) == self.numMotors*2+1:
                    for i in range(self.numMotors):
                        self.positionState[i] = int(cb[2*i])
                        self.velocityState[i] = int(cb[2*i+1])
                        self.timer_reciv_data = time.time()
                        self.ard.flushInput()
                else:
                    self.ard.flushInput()
            except:
                debug("WARNING", "ARDUINO DATA CANNOT BE READ FROM:" + str(self.name))
                pass





    def sendCmd(self, positions, velocities):
        cmd = ""
        for i in range(self.numMotors):
            cmd += str(int(positions[i])) + "," + str(int(velocities[i])) + ","
        cmd = cmd[:-1] + "\n"
        self.ard.write(cmd)


