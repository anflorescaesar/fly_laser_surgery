# Fly laser surgery
<img src="images/description.png"  width="1000" height="200">


<img src="images/laser_cut.gif"  width="500" height="300">



## Description
To record neural activity in behaving flies, a window for imaging through the cuticle was cut in [[1]](#1) using a laser. We use a focused visible continuous wave laser (Lassos Lasertechnik, YLK Series, 561 nm)  with comparatively low power (30 mW is required for cutting) and cost. The fly (glued to a pin with its head fixed with respect to the thorax) is moved against the stationary laser focus using a cost-effective, custom three-axis motorized micromanipulator. A path for the laser was defined in three dimensions on the fly's head, allowing arbitrary geometries for the resulting opening in the cuticle. A single pass through the laser focus is sufficient to cut the cuticle.

A full demo of the laser cut surgery is shown in this [video](images/demo.mp4)


This project contains the instructions and software, with several ROS packages in python to control laser surgery in flies. It requires specific hardware and software listed below. 


## Disclosures
We provide vendors to buy all the components required, but note that there are no conflicts of interest for specifically using the links that we provide. You are welcome to buy similar components from other vendors.



## Hardware requirements
### Micromanipulator

The laser surgery process requires two custom motorized micromanipulators:


- 1) Fly holder micromanipulator: This has attached the fly, so that the fly can be moved in the 3 dimentions to define the cut trajectory


- 2) Eye protector micromanipulator: This has a metal shim with a v-shaped incision (custom made) that is positioned over the over the fly’s head to protect the eyes from laser light during the laser cut.


We built each motorized micromanipulator using existing components and 3D printed parts. All the required components to build one are shown in the following table (remember to get twice the components for 2 micromanipulators):


| Component | Quantity |link to buy | 
| ---      |  ---- | ------  |
| Micromanipulator PT3 | 1 | https://www.thorlabs.de/thorproduct.cfm?partnumber=PT3/M#ad-image-0 |
|Arduino UNO | 1 | https://www.conrad.de/de/p/arduino-ag-mikrocontroller-a000066-uno-rev3-atmega328-1275279.html|
| CNC shield | 1 | https://www.conrad.de/de/p/joy-it-ard-cnc-kit1-motor-shield-1646889.html|
| Nema 17 stepper motor | 3 | https://www.conrad.de/de/p/joy-it-schrittmotor-nema-17-01-nema-17-01-0-4-nm-1-68-a-wellen-durchmesser-5-mm-1597325.html|
| TMC 2130 driver | 3 | https://www.i-love-tec.de/Technik-von-Hilitand/Technik-zu-tmc2130|
| Gt2 Pulley 20 teeth, 5mm Bore | 3 | https://www.amazon.de/Pulley-Timing-Printer-Accessories-Included/dp/B07K79D985/ref=sr_1_2_sspa?dchild=1&keywords=gt2+20+z%C3%A4hne+5+mm+bohrung&qid=1620403218&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFNSDBIN0xKNDBNWTYmZW5jcnlwdGVkSWQ9QTA4ODQ2MjQzVEdBMjM3MjdYWDVFJmVuY3J5cHRlZEFkSWQ9QTA3ODc5MjQyOFJJQlI4OEVYVUhSJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ== |
| Gt2 Timing belt closed loop, 200mm | 3 | https://www.amazon.de/3Dman-Timing-Closed-Rubber-Printers/dp/B07V6N32B1/ref=sr_1_2_sspa?dchild=1&keywords=gt2+zahnriemen+200+mm&qid=1620403279&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEySTJPTDhBUE9XQzNPJmVuY3J5cHRlZElkPUEwNjQ1MDI0M1FWR1ZVVU9OMko3VSZlbmNyeXB0ZWRBZElkPUEwMTEyOTUzMU9VQUdOQU5YVEJNQSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=|

In addition for each micromanipulator, the following parts need to be printed in PLA with a 3D printer:

| 3D model | Quantity | 
| --- | --- |
| [xy motor support](3D_printed_parts/xy_motor_holder.stl) | 1 |
| [xy motor support](3D_printed_parts/xy_motor_holder.stl) | 1 |
| [z motor support](3D_printed_parts/z_motor_holder.stl)   | 1 |
| [Gt2 Pulley 85 teeth](3D_printed_parts/motor_gear_GT2_85_teeth.stl) | 3 |

Note that the parts to support the x and y motors are the same. We recomend to print these parts in the given orientation with 30% infill. Use support material only for the [z motor support](3D_printed_parts/z_motor_holder.stl), and do not use it for the other parts.

Once you have all the components, you can assemble the micromanipulator as in the following figure. Each 3D printed gear is fit in each axis micromanipulator by pressure.

<img src="images/micromanipulator_figure.png"  width="1000" height="700">



### Joystick
Only one joystick is required to control both micromanipulators. The [Joystick controller package](joystick_control/) is configured with the following joystick:

| Component | Quantity |link to buy | 
| ---      |  ---- | ------  |
|Joystick logitech extreme 3D | 1 | https://www.logitechg.com/de-de/products/space/extreme-3d-pro-joystick.942-000031.html |

However other joystick should be also compatible. You can configure the file [joystick settings](joystick_control/scripts/settings.py) to customize how to control the motorized micromanipulators. 

### Optics setup
We built a custom microscope, shown in the following figure:

<img src="images/optical_setup.png"  width="500" height="500">

All the components required to build this microscope are listed in the following table:


| Component | Quantity |link to buy | 
| ---      |  ---- | ------  |
|Lassos Lasertechnik, YLK Series, 561 nm | 1 | https://www.findlight.net/lasers/solid-state-lasers/cw-dpss/lasos-single-frequency-dpss-cw-laser-ylk |
|OlympusPlan N 10x/0.25 air objective | 1 | https://www.edmundoptics.com/p/olympus-pln-10x-objective/29222/ |
| Shutter Thorlabs SH05/M | 1 | https://www.thorlabs.de/thorproduct.cfm?partnumber=SH05/M |
| Lens 1 ACA 10 mm | 1 | https://www.thorlabs.com/thorproduct.cfm?partnumber=AC080-010-A |
| Lens 2 ACA 100 mm | 1 | https://www.thorlabs.com/thorproduct.cfm?partnumber=AC254-100-A |
| Lens 3 ACA 250 mm | 1 | https://www.thorlabs.com/thorproduct.cfm?partnumber=AC254-250-A |
| Polarized beam splitter 50-50%, 10mm | 1 | https://www.thorlabs.com/thorproduct.cfm?partnumber=BS010 |
| Illumination white lamp | 1 | https://www.thorlabs.com/thorproduct.cfm?partnumber=LIUCWHA |



The laser beam is expanded and collimated with two lenses: lens 1 ACA 10 mm and lens 2 ACA 100 mm). The distance between these two lenses is 110 mm. Then the expanded beam is projected into a polarized beam splitter (50-50%) and the reflected laser beam is used to fill the back aperture of the microscope objective (OlympusPlan N 10x/0.25 air objective). A white lamp was used to illuminate the fly and to image its head through the same objective with an additional lens (Lens 3 ACA 250 mm) onto a color camera (Basler acA1920-155uc . 

You need to do a calibration step before cutting. The field of view of the camera has to be in the same focal plane of the laser (after passing through the objective). In addition, you need to make sure that the center of the camera is centered at the focus of the beam.

To cut the cuticle of the fly, the laser power is adjusted to 32 mW at the focal plane. The laser beam is additionally pulsed with a mechanical shutter (Thorlabs SH05/M, opened for 15 milliseconds every 45 milliseconds),resulting in an average power of 8 mW at the focus.


### Cameras 
We used 3 cameras:

- A color camera with a field of view from the microscope that is used to define the keypoints of the cut trajectory

- Two black and white cameras with large field views from the top and the side, to help position the fly at the beginning. 


The following image contains the view from the 3 cameras:

<img src="images/microscope_view.png"  width="500" height="500">

Note that each camera view has a small circle in the center. The blue circle in the top view (microscope view), represents the position of the focused beam (which was previously calibrated). The white circle in the side and top large field views are located in the field of view of the microscope, and are used to position the fly at the beginning, as shown in the next figure:

<img src="images/microscope_view_with_fly.png"  width="500" height="500">

The cameras that we used are listed in the following table:


| Component | Quantity |link to buy | 
| ---      |  ---- | ------  |
|Color camera Basler acA1920-155uc| 1 | https://www.baslerweb.com/en/products/cameras/area-scan-cameras/ace/aca1920-155uc/ |
|Black and white camera Basler acA640-750um| 2 |https://www.baslerweb.com/de/produkte/kameras/flaechenkameras/ace/aca640-750um/?creative=445477317484&keyword=%2Baca640-750um&matchtype=b&network=g&device=c&gclid=Cj0KCQjwytOEBhD5ARIsANnRjVjL2arxH2Y-rcTLL_YBPt6w86P5tLGNJF9aqSyTL3QqNV4G-shrODoaAstAEALw_wcB |





## Software requirements
### ROS

A dedicated desktop computer running Ubuntu 18.04LTS or 19.04LTS is required for this software. Newer versions of Ubuntu will not work, because this project works only with ROS melodic (using python 2.7), and not ROS noetic (which uses python 3.4). This will be fixed in future versions.

The software runs under the framework of ROS (Robot Operating System, https://www.ros.org/). This repository contains ROS packages to control the motorized micromanipulators required for laser surgery. First, install ros melodic:
http://wiki.ros.org/melodic/Installation/Ubuntu


then create a catkin workspace:
http://wiki.ros.org/catkin/Tutorials/create_a_workspace


After this, open a terminal and navigate to your source folder in your catking workspace:

> cd ~/catkin_ws/src/

and clone this project:

> git clone https://gitlab.com/anflorescaesar/fly_laser_surgery/

Finally, build the project:

> cd ~/catkin_ws

> catkin_make

### Arduino
Each micromanipulator is controlled with an Arduino UNO, which receives velocity commands from the computer, therefore you need to upload the necessary code to the arduinos. 
First download and install the arduino IDE https://www.arduino.cc/en/software
After the arduino IDE is installed, you need to copy the [stepper motor library](Arduino/libraries/) in your arduino library folder.
Then you need to upload the script [/Arduino/fly_holder_manipulator_driver/fly_holder_manipulator_driver.ino](Arduino/fly_holder_manipulator_driver/fly_holder_manipulator_driver.ino) to the corresponding arduino controlling the fly holder micromanipulator, and the script [Arduino/eye_protector_manipulator_driver/eye_protector_manipulator_driver.ino](/Arduino/eye_protector_manipulator_driver/eye_protector_manipulator_driver.ino) to the other arduino UNO controlling the eye protector micromanipulator.

Make the connections between the arduino UNO and the stepper motors. It is pretty straight forward, just stack the CNC shield on top of the arduino, then stack the 3 TMC2130 drivers on top of the CNC machine (one for each axis). Make sure that the orientation of the TMC2130 drivers is the correct one. Then connect the 3 Nema stepper motors to the CNC shield. Finally, power the CNC shield with a power supply of 12V and at least 4 Amps for each micromanipulator.

<img src="images/cnc_shield.png"  width="500" height="400">


Configure the serial ports of the arduinos by modifiying the file [/fly_laser_surgery/launch/laser_surgery.launch](fly_laser_surgery/launch/laser_surgery.launch) accordingly. Change the values of "fly_holder_manipulator_port"  and "eye_protector_manipulator_port" to the corresponding serial ports in your computer.


### Pylon
Configure the Basler cameras. Plug each camera in a USB 3.0 port and download and instal the Pylon software:
https://www.baslerweb.com/en/sales-support/downloads/software-downloads/

Open the Pylon viewer and configure the parameters for each cameras. Save the config file of each camera (menu camera->save features) into the folder [/pylon_cameras/config](pylon_cameras/config/) with only the serial name of the camera (and not the model), for example '22841582.pfs'. Then modify the file [/pylon_cameras/scripts/settings.py](pylon_cameras/scripts/settings.py) and replace the corresponding serial name of each camera.

Finally install pypylon: 

> pip install pypylon


## Usage
Make sure that everything is connected: 3 cameras, 1 joystick and 2 arduinos for 2 micromanipulators. Then run in a terminal the following command:

> roslaunch fly_laser_surgery laser_surgery.launch

If everything is fine, 3 windows should show: one with the 3 camera views and two command lines, one for each micromanipulator, as in the following figure:

<img src="images/cmd_fly_holder.png"  width="300" height="350">
<img src="images/cmd_eye_protector.png"  width="300" height="350">


You can control each micromanipulator from the corresponding command line window. The micromanipulators have 3 modes to operate:

- STOP: the position micromanipulator cannot be controlled

- MANUAL: the position of the micromanipulator can be controlled with the joystick

- AUTO: the micromanipulator moves automatically along defined keypositions

To cut the cuticle of the fly, set the manual mode in the fly_holder micromanipulator by typing in its command line

> m manual


<img src="images/cmd_manual.png"  width="300" height="350">

Then, move the fly along 6 key positions that will define the cut trajectory. Use the following command to store each keyposition

> p

<img src="images/define_keypoints.gif"  width="500" height="300">

After you press 'p', a new line in yellow should appear in the command line with the stored position of the micromanipulator.

<img src="images/cmd_setpoints.png"  width="300" height="350">

Set the fly_holder micromanipulator to the auto mode:

> m auto

and the fly will start moving through the keypositions that you defined.

Now, set the manual mode in the eye_protector micromanipulator using the other command line window,

> m manual

Using the joystick, move the metal shim with a v-shaped incision over the fly’s head to protect the eyes from laser light during the laser cut:

<img src="images/put_eye_protector.gif"  width="500" height="300">

Finally, open the laser manually using the shutter (Thorlabs SH05/M) which should intermittently open for 15 milliseconds and close for 30 milliseconds. The resulting power in the fly's head should be around 8 mW. You should be able to cut the cuticle of the fly with only one pass through the defined cut trajectory.

After the cut is done, set both micromanipulators to stop mode

> m stop

<img src="images/cmd_stop.png"  width="300" height="350">

If you can to cut the cuticle in another fly, you should delete the defined keypoints in the fly_holder micromanipulator using the following command:

> clr

A full demo of the laser cut surgery is shown in this [video](images/demo.mp4)



## Questions
If you have any questions or problems, send us an e-mail or open a ticket in this repository. Give us feedback if something is not clear, and thanks for using the fly laser surgery!


## References
<a id="1">[1]</a> Flores-Valle A, Honnef R, Seelig JD. Automated long-term two-photon imaging in head-fixed walking Drosophila. J Neurosci Methods. 2022
https://www.sciencedirect.com/science/article/abs/pii/S0165027021003678?via%3Dihub
