
MICROSCOPE_CAMERA_NAME = "microscope"
TOP_CAMERA_NAME        = "top"
BACK_CAMERA_NAME       = "back"


MICROSCOPE_CAMERA_FORMAT = "rgb8"     # Color camera
TOP_CAMERA_FORMAT        = "mono8"    # Black and white camera
BACK_CAMERA_FORMAT       = "mono8"    # Black and white camera



MICROSCOPE_CAMERA  = '22841582.pfs'
TOP_CAMERA         = '22377462.pfs'
BACK_CAMERA        = '22515499.pfs'



PUBLISH_RATE = 30