import numpy as np
import os
from time import localtime, strftime
from datetime import datetime
import cv2


class VideoRecorder(object):
    """
    Class to record video stream and so on...
    """

    MAX_FRAMES_PER_FILE = 10*60*10


    def __init__(self, base_path, base_name):
        self.base_path = base_path
        self.base_name = base_name
        self.file_counter = 0
        self.frame_counter = 0
        try:
            os.stat(base_path)
        except:
            os.makedirs(base_path)
            print 'Logging directory {} created'.format(base_path)

        self.textlog = os.path.join(self.base_path, self.base_name+'.txt')

        with open(self.textlog, 'w') as tl:
            tl.write(datetime.now().strftime("%H-%M-%S-%f") + ' BEGIN\r\n')

        self.fourcc = cv2.VideoWriter_fourcc(*'MJPG')  # cv2.VideoWriter_fourcc() does not exist
        print 'Set up recording', self.base_path, self.base_name


    def add_frame(self, frame, fps):

        if self.frame_counter == 0:
            new_filename = os.path.join(self.base_path, self.base_name) + '_' + str(self.file_counter) + '.avi'
            self.video_writer = cv2.VideoWriter(
                new_filename,
                self.fourcc,
                fps,                          # frames per second
                tuple([frame.shape[1], frame.shape[0]]),
                isColor=True                        #grayscale video not supported
            )

        self.video_writer.write(frame)
        self.frame_counter += 1
        if self.frame_counter == VideoRecorder.MAX_FRAMES_PER_FILE:
            self.video_writer.release()
            self.frame_counter = 0
            self.file_counter += 1


    def close(self):
        print 'Close recording', self.base_name
        self.video_writer.release()


    def __repr__(self):
        return "Recorder for %s" % str(self.base_name)