#!/usr/bin/env python
import rospy
from settings import *
from acquisition import Camera
import rospkg
from cv_bridge import CvBridge
import cv2
import numpy as np
from recorder import VideoRecorder





class PylonCameras:
    def __init__(self):
	self.debug("INFO", "pylon_cameras_node is running!")
        rospack = rospkg.RosPack()
        configDir = rospack.get_path('pylon_cameras') + '/config/'

        cameraMicroscope = Camera(configDir + MICROSCOPE_CAMERA)
        cameraMicroscope.camera.PixelFormat = 'BayerRG8'

        cameraTop  = Camera(configDir + TOP_CAMERA)
        cameraBack = Camera(configDir + BACK_CAMERA)



        self.cameras      = [cameraMicroscope, cameraTop, cameraBack]
        self.camera_names = [MICROSCOPE_CAMERA_NAME, TOP_CAMERA_NAME, BACK_CAMERA_NAME]
        self.formats      = [MICROSCOPE_CAMERA_FORMAT, TOP_CAMERA_FORMAT, BACK_CAMERA_FORMAT]


        #self.cameraPubs = [cameraMicroscopePub, cameraTopPub, cameraBackPub]

        for camera in self.cameras:
            camera.start()

        self.bridge = CvBridge()
        self.mainFrame = np.zeros([480 + 800, 1280, 3], dtype=np.uint8)
        self.quit = False


        self.RECORD = False

        if self.RECORD:
            self.recorder = VideoRecorder('/home/nci_la/surgery_videos/', 'fly_surgery')

    def isNotNone(self, im):
        if im is not None:
            return True
        return False


    """ Code for the main thread of the node """
    def mainThread(self):

        for i in range(len(self.cameras)):
            frame, _ = self.cameras[i].grab_frame()
            name = self.camera_names[i]
            form   = self.formats[i]

            if not self.quit and frame is not None:
                h, w = frame.shape[0:2]

                if form == 'rgb8':
                    # print frame.shape
                    frame = cv2.cvtColor( frame, cv2.COLOR_BAYER_RG2RGB )
                    #frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

                    frame = cv2.circle(frame, (int(w / 2), int(h / 2)), 20, (255, 0, 0), 2)
                    w = 640 * 2
                    h = int(1200 * w / 1920)
                    frame = cv2.resize(frame, (w, h))
                    self.mainFrame[:800, :, :] = frame

                else:
                    frame = cv2.circle(frame, (int(w / 2), int(h / 2)), 10, 255, 0)
                    frame = np.repeat(frame[:, :, np.newaxis], 3, axis=2)
                    if name == 'top':
                        self.mainFrame[800:, :640, :] = frame
                    elif name == 'back':
                        self.mainFrame[800:, 640:, :] = frame

                key = cv2.waitKey(1)
                if key == 27:
                    self.quit = True
                    cv2.destroyAllWindows()

        factor_zoom = 0.7
        # self.recorder.add_frame(self.mainFrame, 25)
        cv2.imshow("surgery_view", cv2.resize(self.mainFrame, (0, 0), fx=factor_zoom, fy=factor_zoom))

        if self.RECORD:
            self.recorder.add_frame(self.mainFrame, 30)



    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('pylon_cameras_node', anonymous=True)
        rate = rospy.Rate(PUBLISH_RATE)    # 10 Hz
        node = PylonCameras()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

