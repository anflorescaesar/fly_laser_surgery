import os
from camera import Camera
from recorder import VideoRecorder
import cv2
from time import localtime, strftime

class Acquisition:

    def __init__(self, camera_settings_files):
        self.cameras = []
        self.recorders = {}
        self.display_enabled = False
        self.record_enabled = False
        for sf in camera_settings_files:
            self.cameras.append(Camera(sf))

    def enable_record(self, dir):
        self.record_enabled = True
        dir = os.path.join(dir, strftime("%Y-%m-%d %H-%M-%S", localtime()))
        try:
            os.stat(dir)
        except:
            os.makedirs(dir)
            print 'Logging directory {} created'.format(dir)
        for c in self.cameras:
            self.recorders[c.name] = VideoRecorder(dir, c.sn)

    def enable_display(self):
        self.display_enabled = True
        for c in self.cameras:
            cv2.namedWindow(c.name)

    def run(self):
        for c in self.cameras:
            print 'START', c.name
            c.start()
        quit = False
        while cv2.waitKey(1) != ord('q') and not quit:
            for c in self.cameras:
                frame = c.grab_frame()
                if self.display_enabled:
                    cv2.imshow(c.name, frame)
                if self.record_enabled:
                    self.recorders[c.name].add_frame(frame)
            if self.display_enabled:
                if any([cv2.getWindowProperty(c.name, 0) < 0 for c in self.cameras]):
                    quit = True
        for name, rec in self.recorders.iteritems():
            rec.close()
        for c in self.cameras:
            c.close()
        cv2.destroyAllWindows()

    @staticmethod
    def thread_function():
        Acquisition._stop = False

        for cam in Acquisition._cameras:
            if cam.start():
                print 'starting', cam.name
            else:
                print 'CAN NOT CONNECT TO', cam.name
                break
        while not Acquisition._stop:
            frames = [cam.grab_frame() for cam in Acquisition._cameras]
            print 'Frame', time.time()
            pass
        for cam in Acquisition._cameras:
            cam.stop()
        Acquisition._log.append('endf')

    @staticmethod
    def start():
        Acquisition._thread.start()

    @staticmethod
    def stop():
        Acquisition._stop = True
        Acquisition._thread.join()