#include <stepper_motor.h>


/************ INTRO SETTINGS ************/

String MANIPULATOR_NAME = "eye_protector";
#define NUM_MOTORS 3
char *motorNames[] = {"X", "Y", "Z"};


#define MIN_SPEED 50  // steps/sec
#define MAX_SPEED 500 // steps/sec


/************** DEFINE PINS **************/
#define ENABLE_PIN    8

short PIN_STEP[NUM_MOTORS] = {2,3,4};
short PIN_DIR[NUM_MOTORS] = {5,6,7};


/************** ROS SETTINGS **************/
float PUBLISH_DATA_FREQ = 0;


/*************** ROS DEFINITIONS ***********/
/*************** ROS DEFINITIONS ***********/
long positionState;
long velocityState;
String positionCmd;
String velocityCmd;
short commaIx;
short prevCommaIx;

StepperMotor* motors[NUM_MOTORS];


String publishMsg = "";          // string to publish data
String inputString = "";         // string to hold incoming data
bool stringComplete = false;  // whether the string is complete

bool startPublishing = false;

void cmdCB(){
  if (stringComplete){

    if (inputString.substring(0,4) == String("info")){
      String infoMsg = MANIPULATOR_NAME + String(",") + String(NUM_MOTORS);
      for(short i=0; i < NUM_MOTORS; i++){
        infoMsg +=  String(",") + String(motorNames[i]);
      }
      Serial.println(infoMsg);
      delay(5);
      inputString = "";
      stringComplete = false;
      return;
    }
    else if (inputString.substring(0,5) == String("start")){
      PUBLISH_DATA_FREQ = inputString.substring(5).toFloat();
      startPublishing = true;
      inputString = "";
      stringComplete = false;
      return;
    }
    commaIx = -1;
    prevCommaIx = -1;
    short motorIx = 0;
    for(short i=0; i < 2*NUM_MOTORS; i++){
        
        commaIx = inputString.indexOf(',', commaIx + 1);
        
        if ((i & 1) == 0){    // Even for position
          positionCmd = inputString.substring(prevCommaIx + 1, commaIx);
        }
        else{
          
          velocityCmd = inputString.substring(prevCommaIx + 1, commaIx);
          //Serial.println(String(positionCmd) + "," + String(velocityCmd));
          
          motors[motorIx]->command(positionCmd.toInt(), velocityCmd.toInt());
          motorIx++;

        }
        prevCommaIx = commaIx;
            
    
    }

    inputString = "";
    stringComplete = false;
  }
    
}

/*
ros::NodeHandle  nh;
sensor_msgs::JointState jointStateMsg;
ros::Publisher jointStatePub("joint_state", &jointStateMsg);
ros::Subscriber<sensor_msgs::JointState> jointCmdSub("joint_cmd", cmdCB);





*/

long timerPublisher;



void setup() {
  Serial.begin(57600);
  
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);

  for(short i=0; i < NUM_MOTORS; i++){
      motors[i] = new StepperMotor(i, PIN_STEP[i], PIN_DIR[i]);
  }
  //motors[0]->command(10000, 200);

  timerPublisher = millis();


}

void loop() {
  for(short i=0; i < NUM_MOTORS; i++){
      motors[i]->run();
  }
    if (startPublishing){
      publishData();
    }
    //delay(15);
    cmdCB();
}


void publishData(){
  long delayTime = millis() - timerPublisher ;

  if (delayTime > 1000/PUBLISH_DATA_FREQ){
        publishMsg = "";
        for(short i=0; i < NUM_MOTORS; i++){
            positionState = motors[i]->getCurrentStepPosition();
            velocityState = motors[i]->getSpeed();
            publishMsg += String(positionState) + "," + String(velocityState) + ",";
        }
        Serial.println(publishMsg);

        timerPublisher = millis();
    }
}



void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
