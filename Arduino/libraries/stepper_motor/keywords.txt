#######################################
# Syntax Coloring STEPPER MOTOR
#######################################

#######################################
# Class (KEYWORD1)
#######################################

motionMode	KEYWORD1
MOVE_SPEED	KEYWORD1
MOVE_STEPS	KEYWORD1
STOPPED 	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################


moveSpeed		KEYWORD2
getId			KEYWORD2
getName			KEYWORD2
getCurrentStepPosition	KEYWORD2
getSpeed		KEYWORD2

