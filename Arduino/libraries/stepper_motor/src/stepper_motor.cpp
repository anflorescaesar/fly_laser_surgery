#include "stepper_motor.h"

StepperMotor::StepperMotor(short id, short stepPin, short dirPin){
	this->id = id;
	this->stepPin = stepPin;
	this->dirPin = dirPin;


	pinMode(this->stepPin, OUTPUT);
	pinMode(this->dirPin,  OUTPUT);

	this->stepState     = false;
	this->timeStar      = 0;
	this->STATE         = STOPPED;
	this->timeSteps     = 0;
	this->requestedStep = 0;
	this->currentStep   = 0;
	this->currentSpeed  = 0;
	this->STATE         = STOPPED;
}


void StepperMotor::runSingleStep(){
        
	long spentTime = micros() - this->timeStar;

	if ( spentTime > this->timeSteps ){

		digitalWrite(this->stepPin, this->stepState);

		if  ( this->stepState ){
			if ( this->currentDir ){this->currentStep += 1;}
			else                   {this->currentStep -= 1;}
		}

		this->stepState = !this->stepState;
		this->timeStar = micros();
	}
}

void StepperMotor::setDir(bool dir){
	this->currentDir = dir;
	digitalWrite(this->dirPin, this->currentDir);
}


void StepperMotor::moveSpeed(int speed){
	if (speed == 0){
		this->stopMotor();
		return;	
	}

	this->currentSpeed = speed;
	this->timeSteps = 1000000/(2*abs(this->currentSpeed)); 

	if (this->currentSpeed > 0 ){
		this->setDir(true);		
	}
	else{
		this->setDir(false);		
	}
	this->timeStar = micros();
	this->STATE    = MOVE_SPEED;
}


void StepperMotor::move2Position(long reqStep, int speed){
	if (speed == 0){
		this->stopMotor();
		return;	
	}
	this->currentSpeed = speed;
	this->requestedStep = reqStep + this->currentStep;
	this->timeSteps = 1000000/(2*abs(this->currentSpeed)); 
	
	if ((this->requestedStep - this->currentStep) > 0 ){
		this->setDir(true);		
	}
	else{
		this->setDir(false);		
	}
	this->timeStar = micros();
	this->STATE    = MOVE_STEPS;
}


void StepperMotor::command(long reqStep, int speed){
	if (reqStep == 0){this->moveSpeed(speed);}
	else{this->move2Position(reqStep, speed);}
}


void StepperMotor::stopMotor(){
	this->STATE = STOPPED;
	this->currentSpeed = 0;
}


void StepperMotor::run(){
	if ( this->STATE == MOVE_SPEED){
		this->runSingleStep();
	}
	if ( this->STATE == MOVE_STEPS){

		if ( this->currentStep == this->requestedStep ){
			this->stopMotor();	
		}
		this->runSingleStep();
		
		
	}
}






